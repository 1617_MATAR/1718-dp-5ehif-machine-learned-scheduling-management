// http://vuejs-templates.github.io/webpack
var path = require('path')

module.exports = {
  build: {
    env: require('./prod.env'),
    index: path.resolve(__dirname, '../dist/index.html'),
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    productionSourceMap: true,
    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],
    bundleAnalyzerReport: process.env.npm_config_report
  },
  dev: {
    env: require('./dev.env'),
    port: 8090,
    autoOpenBrowser: true,
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {},
    cssSourceMap: false,
    db: {
      host: 'sheepblue.database.windows.net',
      database: 'sheepbluedb',
      username: 'sheepblue',
      password: 'blue_sheep654',
      users: 'sheepblue_user'
    },
    server: {
      // host: 'localhost'
      host: 'sheepblue.westeurope.cloudapp.azure.com'
    },
    defaultvalues: {
      user: {
        version: 0,
        bonus_factor: 0,
        linreg_model_offset: 0.1,
        linreg_model_slope: 0.7,
        lowdat_model_avgresponseprob: 0.5
      }
    }
  }
}
