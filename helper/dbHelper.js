const mssql = require('mssql')
const config = require('../config/index')

const configuration = {
  user: config.dev.db.username,
  password: config.dev.db.password,
  server: config.dev.db.host,
  database: config.dev.db.database,
  options: {
    encrypt: true
  }
}
module.exports = class DatabaseImpl {
  getAllUsers() {
    return new Promise((resolve, reject) => {
      this.executeQuery('select * from sheepblue_user')
        .then(recordset => {
          resolve(JSON.stringify(recordset.recordsets[0]))
        })
        .catch(function(err) {
          console.log(err)
        })
    })
  }
  CountAllUsers() {
    return new Promise((resolve, reject) => {
      this.executeQuery('select count(*) as numberOfUsers from sheepblue_user')
        .then(recordset => {
          resolve(JSON.stringify(recordset.recordsets[0][0]))
        })
        .catch(function(err) {
          console.log(err)
        })
    })
  }
  getUser(id) {
    return new Promise((resolve, reject) => {
      // TODO change to prepared statements

      this.executeQuery('select * from sheepblue_user where id = ' + id)
        .then(recordset => {
          resolve(JSON.stringify(recordset.recordsets[0]))
        })
        .catch(function(err) {
          console.log(err)
        })
    })
  }
  createUser(user) {
    return new Promise((resolve, reject) => {
      this.executeQuery(
        'insert into sheepblue_user (version, bonus_factor, email, firstname, lastname, linreg_model_offset, linreg_model_slope, lowdat_model_avgresponseprob, title, username) ' +
          "values('" +
          config.dev.defaultvalues.user.version +
          "', '" +
          config.dev.defaultvalues.user.bonus_factor +
          "', '" +
          user.email +
          "', '" +
          user.firstname +
          "', '" +
          user.lastname +
          "', '" +
          config.dev.defaultvalues.user.linreg_model_offset +
          "', '" +
          config.dev.defaultvalues.user.linreg_model_slope +
          "', '" +
          config.dev.defaultvalues.user.lowdat_model_avgresponseprob +
          "', '" +
          user.title +
          "', '" +
          user.username +
          "')"
      )
        .then(_ => {
          resolve(true)
        })
        .catch(function(err) {
          console.log(err)
        })
    })
  }
  updateUser(user) {
    return new Promise((resolve, reject) => {
      this.executeQuery(
        'select version from sheepblue_user where id = ' + user.id
      )
        .then(recordsets => {
          let version = parseInt(recordsets.recordset[0].version) + 1
          console.log(
            'update sheepblue_user set version= ' +
              version +
              ", email= '" +
              user.email +
              "', firstname= '" +
              user.firstname +
              "', lastname= '" +
              user.lastname +
              "', title= '" +
              user.title +
              "', username= '" +
              user.username +
              "' where id = " +
              user.id
          )
          this.executeQuery(
            'update sheepblue_user set version= ' +
              version +
              ", email= '" +
              user.email +
              "', firstname= '" +
              user.firstname +
              "', lastname= '" +
              user.lastname +
              "', title= '" +
              user.title +
              "', username= '" +
              user.username +
              "' where id = " +
              user.id
          )
            .then(_ => {
              resolve(true)
            })
            .catch(function(err) {
              console.log(err)
            })
        })
        .catch(function(err) {
          console.log(err)
        })
    })
  }
  deleteUser(userid) {
    return new Promise((resolve, reject) => {
      this.executeQuery('delete from sheepblue_user where id = ' + userid)
        .then(recordset => {
          resolve(true)
        })
        .catch(function(err) {
          console.log(err)
        })
    })
  }
  getUserPreferenceGroups(id) {
    return new Promise((resolve, reject) => {
      this.executeQuery(
        'select preference_group.id, preference_group.type from preference_group ' +
          'inner join sheepblue_user on preference_group.user_id = sheepblue_user.id ' +
          'where sheepblue_user.id = ' +
          id
      )
        .then(recordset => {
          resolve(JSON.stringify(recordset.recordsets[0]))
        })
        .catch(function(err) {
          console.log(err)
        })
    })
  }
  getUserPreferences(preferenceGroupId) {
    return new Promise((resolve, reject) => {
      this.executeQuery(
        'select preference.id, preference.version, preference.normalized_rating, preference.rating, preference.type, preference.preference_group_id from preference ' +
          'inner join preference_group on preference_group.id = preference.preference_group_id ' +
          'where preference.preference_group_id = ' +
          preferenceGroupId
      )
        .then(recordset => {
          resolve(JSON.stringify(recordset.recordsets[0]))
        })
        .catch(function(err) {
          console.log(err)
        })
    })
  }
  async executeQuery(query) {
    const pool = new mssql.ConnectionPool(configuration)
    pool.on('error', err => {
      console.log(err)
    })
    try {
      await pool.connect()
      let result = await pool.request().query(query)
      return result
    } catch (err) {
      console.log(err)
    } finally {
      pool.close()
    }
  }
}
