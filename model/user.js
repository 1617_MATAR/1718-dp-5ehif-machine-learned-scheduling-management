module.exports = {
  user: {
    id: '',
    version: '',
    bonus_factor: '',
    email: '',
    firstname: '',
    lastname: '',
    linreg_model_offset: '',
    linreg_model_slope: '',
    lowdat_model_avgresponseprob: '',
    title: '',
    username: ''
  }
}
