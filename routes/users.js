const express = require('express')
const router = express.Router()
const DatabaseImpl = require('../helper/dbHelper')

router.get('/', function(req, res) {
  res.setHeader('Content-Type', 'application/json')
  if (!this.dbImpl) {
    this.dbImpl = new DatabaseImpl()
  }

  // connection doesn't work properly, need to refresh a site twice at first run.

  this.dbImpl
    .getAllUsers()
    .then(data => res.send(data))
    .catch(console.error)
})
router.post('/getuser', function(req, res) {
  const id = req.body.id
  res.setHeader('Content-Type', 'application/json')
  if (!this.dbImpl) {
    this.dbImpl = new DatabaseImpl()
  }
  this.dbImpl
    .getUser(id)
    .then(data => res.send(data))
    .catch(console.error)
})
router.post('/getpreferencegroups', function(req, res) {
  const id = req.body.id
  res.setHeader('Content-Type', 'application/json')
  if (!this.dbImpl) {
    this.dbImpl = new DatabaseImpl()
  }
  this.dbImpl
    .getUserPreferenceGroups(id)
    .then(data => res.send(data))
    .catch(console.error)
})
router.post('/getpreferences', function(req, res) {
  const id = req.body.id
  res.setHeader('Content-Type', 'application/json')
  if (!this.dbImpl) {
    this.dbImpl = new DatabaseImpl()
  }
  this.dbImpl
    .getUserPreferences(id)
    .then(data => res.send(data))
    .catch(console.error)
})
router.post('/create', function(req, res) {
  const user = req.body
  res.setHeader('Content-Type', 'application/json')
  if (!this.dbImpl) {
    this.dbImpl = new DatabaseImpl()
  }
  this.dbImpl
    .createUser(user)
    .then(created => {
      if (created) {
        res.status(200)
        res.send(created)
      }
    })
    .catch(console.error)
})
router.post('/delete', function(req, res) {
  const userid = req.body.id
  res.setHeader('Content-Type', 'application/json')
  if (!this.dbImpl) {
    this.dbImpl = new DatabaseImpl()
  }
  this.dbImpl
    .deleteUser(userid)
    .then(created => {
      if (created) {
        res.status(200)
        res.send(created)
      }
    })
    .catch(console.error)
})

router.post('/update', function(req, res) {
  const user = req.body
  res.setHeader('Content-Type', 'application/json')
  if (!this.dbImpl) {
    this.dbImpl = new DatabaseImpl()
  }
  this.dbImpl
    .updateUser(user)
    .then(updated => {
      if (updated) {
        res.status(200)
        res.send(updated)
      }
    })
    .catch(console.error)
})

router.get('/count', function(req, res, next) {
  res.setHeader('Content-Type', 'application/json')
  if (!this.dbImpl) {
    this.dbImpl = new DatabaseImpl()
  }

  this.dbImpl
    .CountAllUsers()
    .then(data => res.send(data))
    .catch(console.error)
})

module.exports = router
