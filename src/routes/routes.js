import Layout from '../components/Dashboard/Layout.vue'
// GeneralViews
import NotFound from 'src/components/404.vue'

// Admin pages
import Overview from 'src/components/Dashboard/Overview.vue'
import Users from 'src/components/Dashboard/Users/Users.vue'
import EditUser from 'src/components/Dashboard/Users/EditUser.vue'
import User from 'src/components/Dashboard/Users/User.vue'
import NewUser from 'src/components/Dashboard/Users/NewUser.vue'
import Game from 'src/components/Dashboard/Game.vue'

const routes = [
  {
    path: '/',
    component: Layout,
    redirect: '/admin/dashboard'
  },
  {
    path: '/admin',
    component: Layout,
    redirect: '/admin/dashboard',
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: Overview
      },
      {
        path: 'game',
        name: 'Gamified Preferences',
        component: Game
      },
      {
        path: 'users',
        name: 'Liste Ihrer Angestellten',
        component: Users
      },
      {
        path: 'user/:id/edit',
        name: 'Angestellte bearbeiten',
        component: EditUser
      },
      {
        path: 'user/:id',
        name: 'Angestellter',
        component: User
      },
      {
        path: 'new',
        name: 'Neuen Angestellten anlegen',
        component: NewUser
      }
    ]
  },
  { path: '*', component: NotFound }
]

export default routes
