delete from [dbo].[preference_group];

insert into [dbo].[preference_group] values (1, 0, 'WEATHER', 0, 1)
insert into [dbo].[preference_group] values (1, 0, 'DAYTIME', 0, 1)
insert into [dbo].[preference_group] values (1, 0, 'RELATIONSHIPS', 0, 1)
insert into [dbo].[preference_group] values (1, 0, 'BRANCH', 0, 1)
insert into [dbo].[preference_group] values (1, 0, 'DISTANCE_TO_HOLIDAY', 0, 1)
insert into [dbo].[preference_group] values (1, 0, 'SHIFT_LENGTH', 0, 1)
insert into [dbo].[preference_group] values (1, 0, 'SEASON', 0, 1)
insert into [dbo].[preference_group] values (1, 0, 'WORKTIME_DENSITY', 0, 1)
insert into [dbo].[preference_group] values (1, 0, 'WORKLOAD', 0, 1)

select * from [dbo].[preference_group]
